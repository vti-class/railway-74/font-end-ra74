$(function() {
    // Khi load trang, thì sẽ được chạy vào đây
    checkLogin();
    $("#body").load("./html/account-page.html");
    getListAccount();
});

let apiBaseAccount = 'https://64e5f99609e64530d17f5cb0.mockapi.io/user';
let accountList = [];

function checkLogin(){
    // Check login
    let role = localStorage.getItem("role");
    if(role){
        // NẾu đã đăng nhập // logic here
        if(role == "ADMIN"){
            document.getElementById("navDepartment").style.display = "block";
            // Khi code được load tới đây, thì trang html của account chưa load ra, vì thế
            // thời điểm load tới đây chưa có thẻ nào có id là buttonAccount
            // document.getElementById("buttonAccount").style.display = "block";
        }
    } else{
        window.location = "http://127.0.0.1:5500/login.html";
    }
}

function logout(){
    // localStorage.removeItem("")
    localStorage.clear();
    window.location = "http://127.0.0.1:5500/login.html";
}

function clickNavihome(){
    console.log("Vào trang home")
    $("#body").load("./html/home-page.html");
}

function clickNaviViewListAccount(){
    $("#body").load("./html/account-page.html");
}


function getListAccount() {
    // Lấy ra được danh sách Account từ API
    // Gán danh sách accout vào biến accountList
    $.ajax({
      url: apiBaseAccount,
      type: "GET",
      contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
      // data: JSON.stringify(request),
      error: function (err) {
        // Hành động khi apii bị lỗi
        console.log(err);
        alert(err.responseJSON);
      },
      success: function (data) {
        // Hành động khi thành công
        fillAccountToTable(data);
        console.log(data);
      },
    });
  }
  
  function fillAccountToTable(data) {
    accountList = data;
    $("tbody").empty();
  
    accountList.forEach(function (item, index) {
        let action = "";
        if(localStorage.getItem('role') == 'ADMIN'){
            action = `<i class="fa fa-pencil pointer" style="font-size: 36px; color: orange"></i>
            <i class="fa fa-trash pointer" style="font-size: 36px; color: red" data-toggle="modal" 
            data-target="#modalDelete" onclick="confirmDelete(${item.id})"></i>`
        }
      $("tbody").append(
        `<tr>
        <th scope="row">${index + 1}</th>
        <td>
          <img
            src="${item.avatar}"
            style="width: 100px"
          />
        </td>
        <td>${item.name}</td>
        <td>${item.address}</td>
        <td>${item.createdAt}</td>
        <td>
          ${action}
        </td>
      </tr>`
      );
    });
  }

  function Account(username, avatar, address, createDate){
    this.name = username;
    this.avatar = avatar;
    this.address = address;
    this.createdAt = createDate;
  }

  function onSave(){
    let username = document.getElementById("username").value;
    let avatar = document.getElementById("avatar").value;
    let address = document.getElementById("address").value;
    let createDate = document.getElementById("createDate").value;

    // console.log(username, avatar, address, createDate)
    // Call API thêm mới
    // Tạ object với các dữ liệu như trên.
    let account = new Account(username, avatar, address, createDate);
    console.log(account);
    $.ajax({
        url: apiBaseAccount,
        type: "POST",
        contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
        data: JSON.stringify(account),
        error: function (err) {
          // Hành động khi apii bị lỗi
          console.log(err);
          alert(err.responseJSON);
        },
        success: function (data) {
          // Hành động khi thành công
          // dùng js để tạo 1 click vào nút close
          document.getElementById("modalAccount").click();
        //   $('#modalAccount').hide()
          getListAccount();
        },
      });
  }

  function confirmDelete(id){
    // console.log(id)
    document.getElementById('accountIdDelete').value = id;
  }

  function deleteAccount(){
    let id = document.getElementById('accountIdDelete').value;
    // Call API xoá Account
    $.ajax({
        url: apiBaseAccount + "/" + id,
        type: "DELETE",
        contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
        // data: JSON.stringify(account),
        error: function (err) {
          // Hành động khi apii bị lỗi
          console.log(err);
          alert(err.responseJSON);
        },
        success: function (data) {
          // Hành động khi thành công
          // dùng js để tạo 1 click vào nút close
          showAlrtSuccess("Đã xoá account thành công");
          document.getElementById("modalDelete").click();
          getListAccount();
        },
      });
  }

  // Tạo 1 hàm chung để thông báo thành công
function showAlrtSuccess(text) {
    document.getElementById("text-modal-success").innerText = text;
    $("#modal-success").fadeTo(2000, 500).slideUp(500, function () {
        $("#modal-success").slideUp(5000);
    });
}
